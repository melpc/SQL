package com.pc.melanypatino.sqlmel;

import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class Main2Activity extends AppCompatActivity {

    EditText editTextCodigo, editTextPlaca, editTextPlaza, editTextFecha, editTextHora;
    Button buttonIngresar, buttonModificar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        editTextCodigo = (EditText)findViewById(R.id.editTextCodigo);
        editTextPlaca = (EditText) findViewById(R.id.editTextPlaca);
        editTextPlaza = (EditText) findViewById(R.id.editTextPlaza);
        editTextFecha = (EditText) findViewById(R.id.editTextFecha);
        editTextHora = (EditText) findViewById(R.id.editTextHora);

        buttonIngresar = (Button) findViewById(R.id.buttonIngresar);
        buttonIngresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Registrar(v);
            }
        });

        buttonModificar = (Button) findViewById(R.id.buttonModificar);
        buttonModificar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Modificar(v);
            }
        });
    }

    public void Registrar(View view){

        DBHelper admin = new DBHelper(this);

        SQLiteDatabase db = admin.getWritableDatabase();

        String codigo = editTextCodigo.getText().toString();
        String placa = editTextPlaca.getText().toString();
        String plaza = editTextPlaza.getText().toString();
        String fecha = editTextFecha.getText().toString();
        String hora = editTextHora.getText().toString();

        if (!codigo.isEmpty() && !placa.isEmpty() && !plaza.isEmpty() && !fecha.isEmpty() && !hora.isEmpty()){
            ContentValues registro = new ContentValues();
            registro.put(DBManager.ID, codigo);
            registro.put(DBManager.PLACA, placa );
            registro.put(DBManager.PLAZA, plaza);
            registro.put(DBManager.FECHA, fecha);
            registro.put(DBManager.HORA, hora);

            db.insert(DBManager.TABLE_NAME, null, registro);

            db.close();

            editTextCodigo.setText("");
            editTextPlaca.setText("");
            editTextPlaza.setText("");
            editTextFecha.setText("");
            editTextHora.setText("");

            //Mensaje para decirle al usuario que el registro fue exitoso
            Toast.makeText(this, "Ingreso exitoso", Toast.LENGTH_LONG).show();
            finish();
        }else{
            //Se envia el toast con el mensaje
            Toast.makeText(this, "Debe llenar todos los campos", Toast.LENGTH_LONG).show();
        }


    }

    public void Modificar (View view){

        DBHelper admin = new DBHelper(this);
        SQLiteDatabase db = admin.getWritableDatabase();

        String codigo = editTextCodigo.getText().toString();
        String placa = editTextPlaca.getText().toString();
        String plaza = editTextPlaza.getText().toString();
        String fecha = editTextFecha.getText().toString();
        String hora = editTextHora.getText().toString();

        if (!codigo.isEmpty() && !placa.isEmpty() && !plaza.isEmpty() && !fecha.isEmpty() && !hora.isEmpty()){
            ContentValues registro = new ContentValues();
            registro.put("codigo", codigo);
            registro.put("placa", placa);
            registro.put("plaza", plaza);
            registro.put("fecha", fecha);
            registro.put("hora", hora);

            int cantidad = db.update(DBManager.TABLE_NAME, registro, "codigo="+codigo, null);
            db.close();

            if (cantidad == 1) {
                Toast.makeText(this, "Codigo modificado exitosamente", Toast.LENGTH_LONG).show();
                editTextCodigo.setText("");
                editTextPlaca.setText("");
                editTextPlaza.setText("");
                editTextFecha.setText("");
                editTextHora.setText("");

            }else{
                Toast.makeText(this, "El codigo no existe", Toast.LENGTH_LONG).show();
            }

        }else{

            Toast.makeText(this, "Debe llenar todos los campos", Toast.LENGTH_LONG).show();
        }
    }

}
