package com.pc.melanypatino.sqlmel;

public class DBManager {

    public static final String TABLE_NAME = "vehiculos";
    public static final String ID = "codigo";
    public static final String PLACA = "placa";
    public static final String PLAZA = "plaza";
    public static final String FECHA = "fecha";
    public static final String HORA = "hora";

    public static final String CREATE_TABLE = "create table " +TABLE_NAME+ " ("
            + ID + " integer primary key,"
            + PLACA + " text not null,"
            + PLAZA + " text not null,"
            + FECHA + " text not null,"
            + HORA + " text not null); ";



}
